package ru.t1.nikitushkina.tm.api.model;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    String getUserId();

    void execute();

}
